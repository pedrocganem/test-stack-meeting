import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:golden_toolkit/golden_toolkit.dart';
import 'package:test_stack_meeting/main.dart';

void main() {
  testGoldens("", (tester) async {
    /// loads app fonts
    await loadAppFonts();

    final deviceBuilder = DeviceBuilder()

      /// Setup devices
      ..overrideDevicesForAllScenarios(
        devices: [
          Device.phone,
          Device.iphone11,
          Device.tabletLandscape,
          Device.tabletPortrait,
        ],
      )

      /// Without interactions.
      ..addScenario(
          widget: const MyHomePage(title: "My Home Page"),
          name: "Home page fresh start")

      /// Add Button Tapped twice.
      ..addScenario(
        widget: const MyHomePage(title: "My Home Page"),
        name: "Home page with button tapped twice",
        onCreate: (scenarioWidgetKey) async {
          /// Search e Locate widget
          final addButton = find.descendant(
              of: find.byKey(scenarioWidgetKey),
              matching: find.byIcon(Icons.add));
          expect(addButton, findsOneWidget);

          /// Tap the button twice.
          await tester.tap(addButton);
          await tester.tap(addButton);

          /// Refresh the widget's state
          await tester.pump();
        },
      )

      /// HomePage's textfield with Text inside of it.
      ..addScenario(
          widget: const MyHomePage(title: "My Home Page"),
          name: "TextField with text",
          onCreate: (scenarioKeyWidget) async {
            /// Search e Locate TextField
            final textField = find.descendant(
                of: find.byKey(scenarioKeyWidget),
                matching: find.byType(TextField));
            expect(textField, findsOneWidget);

            /// Enter text in TextField
            await tester.enterText(textField, "EAI GALERA");
            
            /// Refresh widget's state
            await tester.pump();
          });

    ///Update deviceBuilder state
    await tester.pumpDeviceBuilder(deviceBuilder);

    ///Run test
    await screenMatchesGolden(tester, "multi_scenario_home_page_test");
  });
}
